---
title: "Soporte Técnico"
categories:
    - soporte tecnico
date: 2018-01-07T01:27:52-06:00
toc: false
tags:
    - soporte tecnico
---

Brindamos el servicio de soporte técnico por proyecto y/o a negocios que no
tengan un departamento tal cual. ¿Buscas comprar o extender tu equipo nuevo?
[Contáctanos]({{% relref "#contacto"  %}}) y te ayudamos con lo necesario
para ponerlo a trabajar.

{{% figure
    src="/images/soporte-tecnico.png"
    link="/images/soporte-tecnico.png"
    alt="Personal de soporte técnico"
    title="Soporte técnico a equipos"
    caption="¡Soporte técnico remoto por proyecto!"
%}}

Para garantizar el funcionamiento de tu equipo puedes obtener **mantenimiento
preventivo y correctivo de manera física o remota** ¡Te asesoramos a coordinar
el proceso para llevarlo a cabo! Estamos decididos a impulsar los negocios
regiomontanos ofreciendo el mejor servicio de el mercado y más allá con
**soporte remoto y virtual**.
