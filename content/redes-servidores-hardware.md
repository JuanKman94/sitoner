---
title: "Redes, Servidores y Hardware"
categories:
    - redes
    - servidores
    - hardware
date: 2018-01-11T10:54:25-06:00
toc: false
tags:
    - redes
    - servidores
    - hardware
    - gaming
---

Brindamos asesoría para todos los componentes que necesitas cómo **switches
servidores, Access Points, etc.** para armar una infraestructura completa.
Tenemos el mejor servicio hardware desde la obtención del equipo hasta el
licenciamiento y soporte.

{{% figure
    src="/images/hardware.png"
    link="/images/hardware.png"
    alt="PC gamer personalizada"
    title="Armado de PCs"
    caption="Armamos equipo especializado para el uso que necesites."
%}}

[Pide cotización]({{% relref "#contacto" %}}) y empieza a armar tu propio
centro de cómputo con nuestra ayuda.

* Fibra óptica
* Routers
* Teléfonos IP
* Cableado UTP
* Discos de almacenamiento
* UPS (no-break)

Tenemos trato directo con los mejores proveedores y ofrecemos precios muy
competitivos para todos nuestros clientes, haciéndonos siempre un proveedor
confiable y constante.
