---
date: 2017-11-01T01:14:43Z
title: Términos y Condiciones
toc: false
---

El uso de este sitio se rige bajo las siguientes políticas, términos y
condiciones. Favor de leerlas cuidadosamente.

* El uso de este sitio indica su aceptación a estas condiciones y políticas.
* El poner una orden indica su aceptación a estas condiciones y políticas.
* El comprar la orden en SIToner indica su aceptación a estas condiciones y
políticas.

SIToner se reserva el derecho de hacer cambios en este sitio y
en estos términos y condiciones en cualquier momento.

## Política de privacidad

Nosotros mantenemos la información que usted nos proporciona en absoluta
confidencialidad y seguridad. Al hacer una compra en nuestra tienda, usted
nos provee su nombre, correo electrónico, información de su tarjeta de
crédito, teléfono y una contraseña. Nosotros utilizamos esta información
para procesar y mantener actualizada su orden además de personalizar su
experiencia de compra.

Para mantenerlos informados de nuestras ofertas, le notificaremos acerca de
nuestras promociones, nuevos productos y todo lo relacionado con este sitio
de SIToner. a través de un boletín. Usted puede dejar de recibir este
boletín al momento en que usted lo desee, solamente debe de seguir las
instrucciones en cualquier correo electrónico que reciba de nosotros.

SIToner coopera con departamentos de investigación y oficinas
gubernamentales para identificar aquellas personas que utilizan nuestro
servicio para actividades ilegales. Nos reservamos el derecho de reportar a
estos departamentos cualquier actividad(es) que creamos puedan ser ilegales.

Favor de verificar periódicamente esta página acerca de actualizaciones y
cambios en nuestra política de privacidad.

## Copyright y Derechos Reservados

Este sitio es propiedad y esta operado por SIToner, a menos que sea
especificado. Todo material mostrado en este sitio, incluyendo texto,
diseños, logos, gráficas, iconos e imágenes son propiedad de SIToner.

Todo software utilizado en este sitio es propiedad de SIToner o aquellos
proveedores de software. Solamente se puede usar el contenido de este sitio
para propósitos de compra o establecer una orden de compra, no para otros
propósitos. Ningún material de este sitio puede ser copiado, reproducido,
modificado, transmitido o distribuido por cualquier forma o manera sin un
permiso escrito por SIToner.

Cualquier uso no autorizado de los materiales
aparecidos en este sitio pueden violar los derechos de copyright, marcas
registradas u otras leyes dando resultados a penalidades civiles y criminales.

## Políticas de Ventas

Todos los productos vendidos en SIToner son nuevos, y tienen garantía
(excepto cuando se exprese en este sitio) la cual puede variar dependiendo del
fabricante y producto.

SIToner no tiene venta de mostrador, para los clientes de Monterrey y área
metropolitana tenemos envío gratuito a su domicilio y el pago puede ser a
contra entrega.

Los pedidos foráneos se mandan por mensajería previa confirmación de su pago
por deposito o transferencia bancaria.

Todos los productos son entregados en la puerta del domicilio y en ningún caso
incluirá instalación, configuración, capacitación, puesta en marcha, o algún
otro producto ó servicio relacionado con el producto que adquirió en
SIToner.

## Errores Tipográficos

En caso de que un producto contenga un precio incorrecto debido a errores
tipográficos o errores de información recibida por nuestros proveedores,
SIToner tiene el derecho de rechazar o cancelar cualquier orden recibida con
el precio incorrecto. SIToner tiene el derecho de rechazar o cancelar
cualquier orden que no haya sido confirmada y que el cargo se haya realizado en
su tarjeta de crédito. Si en su tarjeta de crédito ya se ha realizado el cargo
correspondiente y su orden es cancelada, SIToner inmediatamente bonificará
la cantidad a su cuenta de tarjeta de crédito por la cantidad incorrecta del
precio.

## Hipervínculos (links)

Este sitio puede contener ligas con otros sitios en Internet que son propiedad
y son operados por terceras personas. Damos por entendido que SIToner no
es responsable por la operación o contenido de esos sitios.

## Políticas de Aceptación de Ordenes

El recibir una orden de confirmación en forma electrónica o por algún otro
medio no significa la aceptación de su orden, y no constituye la
confirmación de SIToner de vender. SIToner se reserva el derecho en
cualquier momento después de recibida la orden en aceptar o rechazar su orden
por cualquier razón. SIToner se reserva el derecho en cualquier momento,
después de recibida su orden y sin previo aviso el proveer menor cantidad de
producto de lo que usted ordenó. Todas las ordenes pagadas con tarjeta de
crédito, deben ser autorizadas. Necesitaremos de verificación e información
adicional antes de aceptar cualquier orden.

## Formas de Pago

Tarjetas de Crédito: Aceptamos las tarjetas de crédito: Visa y MasterCard en
Monterrey y área metropolitana, el cargo se hace al momento de la entrega de
los productos delante del cliente para asegurar la seguridad de sus datos.

**Depósitos Bancarios**: SIToner acepta depósitos bancarios solamente en
pesos mexicanos. Las ordenes son procesadas al momento de recibir el depósito.

**Pagos a la entrega**: SIToner acepta pagos a la entrega de la mercancía solo
en la ciudad de Monterrey Nuevo León y su área Metropolitana.

## Política de Envío

SIToner no envía los Domingos. Debido a restricciones por parte la empresa
de paquetería no hay envíos a Apartados Postales. Todos los envíos están
sujetos a disponibilidad en inventario.

## Términos y Precios de Envíos

Los precios de envíos realizados por SIToner se calculan de acuerdo al peso
y dimensión del producto que esta adquiriendo por lo cual tienen variación.
Para saber el precio de mensajería correspondiente al producto, consultar
directamente con SIToner.

## Impuestos

Todos los productos que se venden en SIToner tienen incluido el impuesto al
valor agregado (IVA). Pudiese haber artículos con excepciones.

## Política de Garantía

Los productos pueden ser devueltos para su reemplazo de la siguiente manera.
Las devoluciones a SIToner para tramite de garantía deben de ser dentro del
tiempo que cubre la Garantía. Usted debe de contactar a nuestro departamento
de atención al cliente, debido a que hay productos donde la garantía se
tramita directamente en los centros de Servicio, y otros donde nosotros
tenemos que tramitar dicha garantía. Usted se hace responsable de los gastos
de envío al hacer una devolución, no aceptamos paquetes con cargo.

En productos de Software no hay garantía ni devoluciones.
En caso software defectuoso puede ser devuelto para reemplazo del mismo
artículo.

Todos los artículos que se reciben para garantía deben de estar en su empaque
original y con las tarjetas de garantía, manuales y accesorios. Cualquier
discrepancia puede resultar en demora concerniente a su garantía. Si el
artículo no es devuelto a SIToner con todo su empaque original se devolverá
de nuevo el artículo a usted. Si parte de un producto se encuentra
defectuosos, todo el artículo debe ser devuelto para su reemplazo o reparación.

Los productos que presenten algún daño físico no se recibirán para garantía.

Los productos que presenten un daño debido al mal uso, incompatibilidad o
mala instalación quedaran automáticamente fuera de garantía.

Las garantías son sujetas a aprobación por parte del Proveedor o Fabricante,
si por alguna razón el mayorista cierra o no acepta la garantía SIToner no
se hará responsable de la misma y el artículo sera devuelto en el estado en
que se recibió para tramitar dicha garantía.

En caso de que el producto en garantía presente un buen funcionamiento el
costo de envío a su domicilio correrá por su cuenta, a demás del cargo por
servicio que aplique dependiendo las políticas del fabricante o mayorista
que atendió dicho proceso de garantía.

## No se aceptan Devoluciones ni Cancelaciones.

No hacemos cambio ni devolución por Incompatibilidad, es responsabilidad del
cliente verificar la compatibilidad y ficha técnica del producto en la página
del fabricante, no nos hacemos responsables por errores u omisiones en la
información del producto.

En ciertos productos la garantía es directo con el centro de servicio en
estos casos el tramite lo deberá hacer el cliente directamente con el
centro de servicio autorizado por el fabricante.

## Garantía del Fabricante

Las garantías varían dependiendo del producto y fabricante, así como también
la manera de tramitar dichas garantías, por favor contacte con atención
al cliente para que le informen como debe proceder con la garantía de algún
producto en particular.

La información del fabricante esta sujeto a cambio sin previo aviso.
