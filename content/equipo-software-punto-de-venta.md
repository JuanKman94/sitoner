---
title: "Equipo y Software Punto de Venta"
categories:
    - hardware
    - software
    - punto de venta
date: 2018-01-11T12:06:16-06:00
toc: false
tags:
    - hardware
    - software
    - punto de venta
---

Te ofrecemos paquetes completos de punto de venta que incluye computadora,
monitor táctil, impresora de ticket, lectores de tarjetas o códigos de
barras, etc. o los componentes por separado si así lo prefieres, cuéntanos
tu necesidad y [te cotizamos]({{% relref "#contacto" %}}) una actualización a tu equipo o atractivas
alternativas en las que somos expertos.

{{% figure
    src="/images/equipo-de-venta.png"
    link="/images/equipo-de-venta.png"
    alt="Paquete con todo incluido para tu negocio."
    title="Promociones"
    caption="Obtén un precio especial al comprar el paquete."
%}}

Atiende más rápido a tus clientes usando tu computadora, factura
electrónicamente, maneja varias cuentas a la vez, imprime los tickets de tus
ventas, maneja los créditos y ganancias de tu negocio y controla tu
inventario y promociones de todo tipo.

Entendemos el campo como tu y queremos darte el **mejor producto para crecer tu
negocio**.
