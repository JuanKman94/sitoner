---
title: "Software y Accesorios"
categories:
    - software
    - accesorios
    - laptops
date: 2018-01-11T16:27:58-06:00
toc: false
tags:
    - software
    - accesorios
    - autodesk
    - antivirus
    - laptops
    - gaming
---

Diseñamos equipo con el software necesario para el día a día de toda empresa,
desde **software de CAD** hasta laptops de uso rudo con capacidad de cargas
pesadas, tarjetas de vídeo para computadoras gaming y que manejan producción
de vídeo profesional.

* Kaspersky
* AutoCAD
* Sony Vegas
* Autodesk

Te ayudamos a armar la estación de trabajo de tus sueños con las pantallas,
accesorios y especificaciones necesarias para que no te detengas ni un segundo
consiguiendo el software que necesitas ni horas buscando el equipo correcto
para tu trabajo. [¡Pide una cotización!]({{% relref "#contacto" %}})

{{% figure
    src="/images/autodesk-software.png"
    link="/images/autodesk-software.png"
    alt="Captura de pantalla de AutoCAD"
    title="Software preinstalado"
    caption="Te instalamos los programas que necesites al comprar una computadora."
%}}
