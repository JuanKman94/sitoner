'use strict';

var swiper = require('swiper'),
    sticky = require('./sticky'),
    smoothScroll = require('smooth-scroll'),
    validate = require('validate.js'),
    houdini = require('houdinijs');

window.swiper = swiper;
window.sticky = sticky;
window.smoothScroll = smoothScroll;
window.houdini = houdini;

new sticky({
    target: 'nav',
    offset: document.querySelector('body > header').offsetHeight
});

var mySwiper = new Swiper ('.swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    loop: true,
    effect: 'fade',
    autoplay: 4000,
    speed: 500,

    // Navigation arrows
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    pagination: '.swiper-pagination',
    paginationClickable: true,
});

houdini.init();
var scroll = smoothScroll('a[href*="#"]:not([data-collapse])', {
    speed: 800,
    after: function (target, sauce) {
        target.querySelector('.collapse-toggle').click();
    }
});

// Form validation
var constraints = {
    nombre: {
        presence: true,
        format: {
            pattern: "[a-zA-Z]+",
            flags: "i",
            message: "puede tener únicamente letras"
        },
        length: {
            minimum: 3,
            tooShort: "debe tener al menos %{count} letras"
        }
    },
    telefono: {
        presence: true,
        format: {
            pattern: "[0-9 ]+",
            flags: "",
            message: "puede contener números y espacios."
        },
        length: {
            minimum: 8,
            maximum: 14,
            tokenizer: function (val) {
                return val.replace(/\s/g, '').split('');
            },
            tooShort: "tiene que tener al menos %{count} números.",
            tooLong: "tiene que tener máximo %{count} números."
        }
    }
};

document.forms.contacto.addEventListener('submit', function (ev) {
    var values = validate.collectFormValues(ev.target),
        errors = undefined,
        errorsDiv = document.querySelector('.errors'),
        errorList = document.querySelector('.errors ul');

    function showErrors () {
        errorList.innerHTML = "";
        for (var key in errors) {
            for (var i in errors[key]) {
                errorList.innerHTML += "<li>" + errors[key][i] + "</li>";
            }
        }
    }

    errors = validate(values, constraints);

    if (errors) {
        errorsDiv.classList.remove('hidden');
        showErrors();

        ev.preventDefault();
        ev.stopPropagation();

        return false;
    } else {
        errorsDiv.classList.add('hidden');
        return ev;
    }
});
