---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
category: Placeholder
date: {{ .Date }}
toc: false
tags:
    - {{ replace .TranslationBaseName "-" " " | title }}
---
