# SI Toner

Source code for [sitoner.mx](https://sitoner.mx) using [Hugo](https://gohugo.io).
Built using [docker](https://docker.com), using
[solidnerd's image](https://store.docker.com/community/images/solidnerd/hugo).

To check the spelling I use [aspell](http://aspell.net):

```bash
$ aspell -l es --save-repl --mode=html --dont-backup -c <path>/<to>/<file>
```

## Deployment

Documentation for GitLab's CI can be read
[here](https://gitlab.com/help/ci/yaml/README.md).

## Development

With docker:

```bash
$ cat package.json
{
    ...
    "scripts": {
        "watch": "gulp watch"
    },
    ...
    "dependencies": {
		"gulp": "^3.9.1"
    }
}
$ docker run --rm -it -v $PWD:/usr/src/app -w /usr/src/app node:8.6.0 npm install
$ docker run --rm -it -v $PWD:/usr/src/app -w /usr/src/app node:8.6.0 npm run watch
$ hugo server
```

For the asset pipeline I started adding
[sass](https://discourse.gohugo.io/t/hugo-will-not-rebuild-site-after-syncing-gulp-changes/8607)
support, then
[js](https://github.com/gulpjs/gulp/blob/master/docs/recipes/browserify-uglify-sourcemap.md)
using browserify.

## Third-party services & libraries

* [formspree](https://formspree.io/) for contact form
(it's free!), requires a kind of initial confirmation, I'm guessing it's a
confirmation of origin URL, so fire'n'forget.
* Carousel? [Swiper](https://github.com/nolimits4web/Swiper)
* [Color palette generator](http://www.cssdrive.com/imagepalette/index.php)
* [Normalize](https://github.com/necolas/normalize.css/)
* Creative Commons images [search engine](https://search.creativecommons.org/),
[flickr](https://www.flickr.com/search/?license=4%2C5%2C6%2C9%2C10&text=home%20office%20desk&advanced=1&dimension_search_mode=min&height=1024&width=1024&orientation=landscape&media=photos&min_taken_date=1388556000)
