'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    sass = require('gulp-sass'),
    uglify = require('gulp-uglify'),
    gutil = require('gulp-util'),
    size = require('gulp-size'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    autoprefixer = require('gulp-autoprefixer');

//Variables
var input = './scss/styles.scss',
    watchFiles = './scss/**.scss',
    output = './static/css',
    sassOptions = {
        errLogToConsole: true,
        outputStyle: 'expanded'
    };

var autoprefixerOptions = {
    browsers: [ 'last 10 versions', '> 5%', 'Firefox ESR' ]
};

// ================== Tasks ================== //

gulp.task('sass', function () {
    return gulp
        // Find all `.scss` files from the `stylesheets/` folder
        .src(input)
        // Run Sass on those files
        .pipe(sass(sassOptions).on('error', sass.logError))
        // Write the resulting CSS in the output folder
        .pipe(autoprefixer())
        .pipe(gulp.dest(output));
});

gulp.task('js', function() {
    var b = browserify({
            entries: './js/main.js',
            debug: true
        });

    return b.bundle()
        .pipe(source('./main.js'))
        .pipe(buffer())
        .pipe(uglify())
        .on('error', gutil.log)
        .pipe(size())
        .pipe(gulp.dest('./static/js/'));
});

gulp.task('watch:sass', function() {
    return watch(watchFiles, { name: 'sass' }, function(vinyl) {
        gulp.start('sass');
    });
});

gulp.task('watch:js', function() {
    return watch('./js/main.js', { name: 'js' }, function(vinyl) {
        gulp.start('js');
    });
});

gulp.task('watch', [ 'build' ], function() {
    gulp.start('watch:sass');
    gulp.start('watch:js');
});


gulp.task('build', [ 'sass', 'js' ]);
gulp.task('default', [ 'watch' ]);
